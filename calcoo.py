#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora():

    def __init__(self, op1, operando, op2):
        self.op1 = op1
        self.op2 = op2
        self.operando = operando

    def suma(self):
        return self.op1 + self.op2

    def resta(self):
        return self.op1 - self.op2

    def operar(self):
        if self.operando == "suma":
            return self.suma()
        elif self.operando == "resta":
            return self.resta()
        else:
            sys.exit('Operación sólo puede ser sumar o restar.')


if __name__ == "__main__":
    if len(sys.argv) != 4:
        sys.exit("Error: calcoo.py operando1 operador operador2")

    operando = sys.argv[2]
    try:
        operando1 = float(sys.argv[1])
        operando2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    calc = Calculadora (operando, operando1, operando2)
    resultado = calc.operar()
    print(resultado)
