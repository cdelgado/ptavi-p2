#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit("Usage: calcplus.py fichero")

    fich = open(sys.srgv[1], 'r')
    lineas = fich.readlines()

    for linea in lineas:
        s = linea.split(',')
        try:
            operando = float(s[0])
            operando1 = float(s[1])
            operando2 = float(s[2])
        except ValueError:
            print("Valor introducido no valido")
            continue
        calc = calcoohija.CalculadoraHija(operando, operando1, operando2)
        for opera in s[2:]:
            try:
                operando2 = float(opera)
                calc = CalculadoraHija(operando, resultado, operando2)
                resultado = calc.operar()
            except ValueError:
                print("Valor introducido no valido")
        print(resultado)

    fichero.close()
