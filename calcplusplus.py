#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija
import csv


if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit("Usage: calcplus.py fichero")
    with open(sys.argv[1]) as csv:
        datos = csv.reader(csv)
        for linea in datos:
            operando = linea[0]
            operando1 = float(linea[1])
            operando2 = float(linea[2])
            calc = calcoohija.CalculadoraHija(operando, operando1, operando2)
            resultado = calc.operar()

            for opera in linea[2:]:
                try:
                    operando2 = float(opera)
                    calc = calcoohija.CalculadoraHija(operando, resultado, operando2)
                    resultado = calc.operar()

                except ValueError:
                    print("Valor introducido no valido")
            print(resultado)

    csvarchivo.close()
