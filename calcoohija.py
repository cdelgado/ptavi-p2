#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):
    def multiplicacion(self):
        return self.op1 * self.op2

    def division(self):
        if self.op2 == 0:
            print("Division is not allowed")
        else:
            return self.op1 / self.op2

    def operar(self):
        if self.operando == "suma":
            return self.suma()
        elif self.operando == "resta":
            return self.resta()
        elif self.operando == "multiplicacion":
            return self.multiplicacion()
        elif self.operando == "division":
            return self.division()
        else:
            sys.exit('Operación sólo puede ser sumar,restar,multiplicar o dividir.')


if __name__ == "__main__":
    if len(sys.argv) != 4:
        sys.exit("Error: calcoohija.py operando1 operador operador2")
        

    operando = sys.argv[2]
    try:
        operando1 = float(sys.argv[1])
        operando2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    calc = CalculadoraHija (operando, operando1, operando2)
    resultado = calc.operar()

    print(resultado)
